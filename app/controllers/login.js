import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    selectUser() {
      this.transitionToRoute('lobby');
    }
  }
});
