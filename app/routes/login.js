import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return [
      {
        name: "John",
        id: "1"
      },
      {
        name: "Roger",
        id: "2"
      },
      {
        name: "Thomas",
        id: "3"
      }
    ];
  }
});
