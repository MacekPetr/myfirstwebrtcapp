import Ember from 'ember';

export default Ember.Component.extend({
  init() {
    this.debug = false;
    this.calling = false;
    this.localStream = null;
    this.localPeerConnection = null;
    this.remotePeerConnection = null;
  },

  actions: {
    startCall() {

      window.RTCPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection;

      var constraints = {
        audio: true,
        video: true
      };

      window.constraints = constraints;

      function handleSuccess(stream) {
        this.trace("Received local stream");
        Ember.set(this, "videoSrc", window.URL.createObjectURL(stream));
        Ember.set(this, "calling", true);
        Ember.set(this, "localStream", stream);

        this.trace("Starting call");
        if (stream.getVideoTracks().length > 0) {
          this.trace('Using video device: ' + stream.getVideoTracks()[0].label);
        }
        if (stream.getAudioTracks().length > 0) {
          this.trace('Using audio device: ' + stream.getAudioTracks()[0].label);
        }

        var servers = null;

        var localTest = new RTCPeerConnection(servers);

        Ember.set(this, "localPeerConnection", localTest);
        this.trace("Created local peer connection object localPeerConnection");
        this.localPeerConnection.onicecandidate = this.gotLocalIceCandidate.bind(this);

        Ember.set(this, "remotePeerConnection", new RTCPeerConnection(servers));
        this.trace("Created remote peer connection object remotePeerConnection");
        this.remotePeerConnection.onicecandidate = this.gotRemoteIceCandidate.bind(this);
        this.remotePeerConnection.onaddstream = this.gotRemoteStream.bind(this);

        this.localPeerConnection.addStream(stream);
        this.trace("Added localStream to localPeerConnection");
        this.localPeerConnection.createOffer(this.gotLocalDescription.bind(this), this.handleError.bind(this));
      }

      this.trace("Requesting local stream");
      navigator.mediaDevices.getUserMedia(constraints)
        .then(handleSuccess.bind(this));
      //.catch(handleError);
    },

    stopCall() {
      this.trace("Ending call");
      this.localPeerConnection.close();
      this.remotePeerConnection.close();
      Ember.set(this, "videoSrc", null);
      Ember.set(this, "remoteVideoSrc", null);
      Ember.set(this, "localPeerConnection", null);
      Ember.set(this, "remotePeerConnection", null);
      Ember.set(this, "calling", false);
    }
  },

  gotLocalIceCandidate(event) {
    if (event.candidate) {
      this.remotePeerConnection.addIceCandidate(new RTCIceCandidate(event.candidate));
      this.trace("Local ICE candidate: \n" + event.candidate.candidate);
    }
  },

  gotRemoteIceCandidate(event) {
    if (event.candidate) {
      this.localPeerConnection.addIceCandidate(new RTCIceCandidate(event.candidate));
      this.trace("Remote ICE candidate: \n " + event.candidate.candidate);
    }
  },

  gotRemoteStream(event) {
    Ember.set(this, "remoteVideoSrc", window.URL.createObjectURL(event.stream));
    this.trace("Received remote stream");
  },

  gotLocalDescription(description) {
    this.localPeerConnection.setLocalDescription(description);
    this.trace("Offer from localPeerConnection: \n + description.sdp(trimmed)");
    this.remotePeerConnection.setRemoteDescription(description);
    this.remotePeerConnection.createAnswer(this.gotRemoteDescription.bind(this), this.handleError.bind(this));
  },

  gotRemoteDescription(description) {
    this.remotePeerConnection.setLocalDescription(description);
    this.trace("Answer from remotePeerConnection: \n + description.sdp (trimmed)");
    this.localPeerConnection.setRemoteDescription(description);
  },

  trace(text) {
    this.debug && console.log((performance.now() / 1000).toFixed(3) + ": " + text);
  },

  handleError(error) {
    this.trace("error: ", error);
  }
});
